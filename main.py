from language import Language

if __name__ == "__main__":
    vt = ['a', 'b', 'c', '$']
    vn = ['S', 'A', 'B', 'C']

    p0 = {
        'ABABC': ['AbBc'],
        'A': ['bcA', 'Ba'],
        'B': ['b']
    }

    p1 = {
        'S': ['C$'],
        'C': ['Abb', 'Ba'],
        'Ab': ['a', 'Ca'],
        'B': ['b', 'Cb']
    }

    p2 = {
        'S': ['Cb'],
        'C': ['a', 'aC', 'abcB', '$'],
        'B': ['Bb', 'b']
    }

    p3 = {
        'X': ['$', 'a', 'aY'],
        'Y': ['b']
    }

    s = 'S'
    lang = Language(vt, vn, p3, s)

    print(lang.get_kind())
